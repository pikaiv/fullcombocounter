window.fcc = function(){};
window.fcc.session_id = null;
window.fcc.login_username = null;

$(function () {
	show_login_link = function () {
		$("#header_login_state").html('<a href="javascript:open_login_window()">ログイン</a>');
	}
	setTimeout(show_login_link, 0);
	$(".point-you-container").hide();
	window.fcc.session_id = null;
	window.fcc.login_username = null;
	setTimeout(reload_fullcombo_history, 0);
	setTimeout(disable_regist_button, 0);
});

function open_login_window () {
	$("#login-form-message").html('');
	$("#login-form-row-message").hide();
	$('#login-form').fadeIn();
	$('.overlay').fadeIn();
}

function close_login_window () {
	$('.modal-container').fadeOut();
	$('.overlay').fadeOut();
}

function login_form_key_input () {
	if ( window.event.keyCode == 13 ) {
		setTimeout(exec_login, 0);
	}
}

function exec_login () {
	username = $('[name=login-username]').val();
	password = $('[name=login-password]').val();

	show_error_message = function ( message ) {
		$("#login-form-message").html(message);
		$("#login-form-row-message").show();
	}

	if ( username.length == 0 ) {
		setTimeout(show_error_message('ユーザー名が空欄です。'), 0);
		return;
	}
	if ( password.length == 0 ) {
		setTimeout(show_error_message('パスワードが空欄です。'), 0);
		return;
	}

	header_update = function () {
		$("#header_login_state").html(window.fcc.login_username);
	}

	show_fullcombo_button = function () {
		$(".counter-button-container").show();
		$(".point-you-container").show();
		$(".counter-checkbox-container").show();
		$(".counter-difficulty-select-container").show();
	}

	onSuccess = function ( data, textStatus, jqXHR ) {
		json = JSON.parse(data);
		window.fcc.session_id = json.session_id;
		window.fcc.login_username = json.name;
		setTimeout(close_login_window, 0);
		setTimeout(header_update, 0);
		setTimeout(show_fullcombo_button, 0);
		setTimeout(reload_my_score, 0);
		setTimeout(enable_regist_button, 500);
	}

	onError = function ( data, textStatus, jqXHR ) {
		setTimeout(show_error_message('ログインに失敗しました。'), 0);
	}

	ajax_config = {
		method: 'POST',
		data: {
			'user': username,
			'password': password
		},
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/login', ajax_config);

	$("#login-form-message").html('');
	$("#login-form-row-message").hide();
}

function reload_my_score () {
	if ( (window.fcc.session_id == null) || (window.fcc.login_username == null) ) {
		return;
	}

	onSuccess = function ( data, textStatus, jqXHR ) {
		json = JSON.parse(data);
		$("#point-you-value").html(json.score);
	}

	onError = function ( data, textStatus, jqXHR ) {
	}

	ajax_config = {
		method: 'GET',
		headers: { Authorization: 'Bearer ' + window.fcc.session_id },
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/users/'+window.fcc.login_username, ajax_config);
}

function reload_fullcombo_history () {
	add_table_body = function ( index, val ) {
		display_created_at = convert_to_date_class(val.created_at.date);
		string_created_at = format_date_class(display_created_at);
		if ( val.user == window.fcc.login_username ) {
			delete_link = '<a href="javascript:show_delete_confirmation(\''+val.id+'\')">削除</a>';
		} else {
			delete_link = '削除';
		}
		$("#fullcombo-history-table-body").prepend('<tr><td>'+val.user+'</td><td>'+string_created_at+'</td><td>'+val.difficulty.difficulty_name+'</td><td>'+val.score+'</td><td>'+delete_link+'</td></tr>')
	}

	onSuccess = function ( data, textStatus, jqXHR ) {
		json = JSON.parse(data);
		$("#fullcombo-history-table-body").empty();
		$.each(json, add_table_body);
		setTimeout(reload_fullcombo_history, 5000);
	}

	onError = function ( data, textStatus, jqXHR ) {
	}

	ajax_config = {
		method: 'GET',
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/logs', ajax_config);
}

function exec_regist_fullcombo ( difficulty, score ) {
	disable_regist_button();
	if ( (window.fcc.session_id == null) || (window.fcc.login_username == null) ) {
		return;
	}

	onSuccess = function ( data, textStatus, jqXHR ) {
		setTimeout(reload_my_score, 0);
		setTimeout(enable_regist_button, 1000);
		setTimeout(reset_fc_button, 0);
	}

	onError = function ( data, textStatus, jqXHR ) {
		console.log(data);
		show_modal_error_message('フルコンボの登録に失敗しました。ログインセッションが無効になっているかもしれません。<strong>ページをリロードして、再度ログインしなおしてから</strong>、やりなおしてください。');
		setTimeout(enable_regist_button, 1000);
	}

	ajax_config = {
		method: 'POST',
		headers: { Authorization: 'Bearer ' + window.fcc.session_id },
		data: {
			'score': score,
			'difficulty': difficulty
		},
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/users/'+window.fcc.login_username, ajax_config);
}

function disable_regist_button () {
	$(".fullcombo-button").prop('disabled', true);
}

function enable_regist_button () {
	$(".fullcombo-button").prop('disabled', false);
}

function show_modal_error_message ( message ) {
	// 開いている既存のモーダルをすべて閉じる
	$('.modal-container').fadeOut();
	$('.overlay').fadeOut();

	$('.modal-error-message-show-box').html(message);
	$('#modal-error-message').fadeIn();
	$('.overlay').fadeIn();
}

function close_modal_error_message () {
	$('.modal-container').fadeOut();
	$('.overlay').fadeOut();
}

function show_delete_confirmation (log_id) {
	// 開いている既存のモーダルをすべて閉じる
	$('.modal-container').fadeOut();
	$('.overlay').fadeOut();

	$('.delete-log-detail').empty();
	$('#delete-confirm').fadeIn();
	$('.overlay').fadeIn();

	$('[name=delete-log-id]').attr('value', log_id);

	get_log_detail = function ( log_id ) {
		console.log(log_id);
		$.get('https://fc.yakitamago.info/api/v1/logs/'+log_id, function (data) {
			json = JSON.parse(data);
			$('.delete-log-detail').html(json.user + ', '+ format_date_class(convert_to_date_class(json.created_at.date)) +', '+ json.difficulty.difficulty_name +', '+ json.score);
		});
	}
	setTimeout(get_log_detail(log_id), 0);
}

function close_delete_window () {
	$('.modal-container').fadeOut();
	$('.overlay').fadeOut();
}

function exec_delete () {
	if ( (window.fcc.session_id == null) || (window.fcc.login_username == null) ) {
		return;
	}
	id = $('[name=delete-log-id]').val();
	if ( id.length == 0 ) {
		show_modal_error_message('ログの削除に失敗しました。ログ ID が指定されていません。これはソフトウェアの不具合である可能性があります。');
	}

	onSuccess = function ( data, textStatus, jqXHR ) {
		setTimeout(reload_my_score, 0);
	}

	onError = function ( data, textStatus, jqXHR ) {
		show_modal_error_message('ログの削除に失敗しました。ログインセッションが無効になっているかもしれません。<strong>ページをリロードして、再度ログインしなおしてから</strong>、やりなおしてください。');
	}

	ajax_config = {
		method: 'DELETE',
		headers: { Authorization: 'Bearer ' + window.fcc.session_id },
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/logs/'+id, ajax_config);
	setTimeout(close_delete_window, 0);
}

function convert_to_date_class ( datetime_string ) {
	return_data = new Date();
	return_data.setUTCFullYear(Number(datetime_string.substr(0, 4)));
	return_data.setUTCMonth(Number(datetime_string.substr(5, 2))-1);
	return_data.setUTCDate(Number(datetime_string.substr(8, 2)));
	return_data.setUTCHours(Number(datetime_string.substr(11, 2)));
	return_data.setUTCMinutes(Number(datetime_string.substr(14, 2)));
	return_data.setUTCSeconds(Number(datetime_string.substr(17, 2)));
	return return_data;
}

function format_date_class ( datetime_class ) {
	return_data = datetime_class.getFullYear() + '/' + (datetime_class.getMonth()+1) + '/' + datetime_class.getDate() + ' ';
	return_data += datetime_class.getHours() + ':';
	return_data += ((datetime_class.getMinutes() < 10) ? '0' + datetime_class.getMinutes() : datetime_class.getMinutes()) + ':';
	return_data += ((datetime_class.getSeconds() < 10) ? '0' + datetime_class.getSeconds() : datetime_class.getSeconds());
	return return_data;
}

function convert_difficulty_from_score ( score ) {
	difficulty = 'unknown';
	switch ( score ) {
		case 1:
			difficulty = 'Pro';
			break;
		case 2:
			difficulty = 'Master';
			break;
		default:
			difficulty = 'Unknown';
			break;
	}
	return difficulty;
}

function toggle_checkbox_first () {
	isChecked = function () { return $("#checkbox-first-input").prop('checked'); };
	$("#checkbox-first-input").prop('checked', !isChecked());
	$("#checkbox-first-show").html(isChecked() ? '&#x2705;' : '&#x274C;');
}

function toggle_checkbox_second () {
	isChecked = function () { return $("#checkbox-second-input").prop('checked'); };
	$("#checkbox-second-input").prop('checked', !isChecked());
	$("#checkbox-second-show").html(isChecked() ? '&#x2705;' : '&#x274C;');
}

function toggle_checkbox_third () {
	isChecked = function () { return $("#checkbox-third-input").prop('checked'); };
	$("#checkbox-third-input").prop('checked', !isChecked());
	$("#checkbox-third-show").html(isChecked() ? '&#x2705;' : '&#x274C;');
}

function toggle_checkbox_encore () {
	isChecked = function () { return $("#checkbox-encore-input").prop('checked'); };
	$("#checkbox-encore-input").prop('checked', !isChecked());
	$("#checkbox-encore-show").html(isChecked() ? '&#x2705;' : '&#x274C;');
}

function exec_submit () {
	// チェックボックスの状態を取得
	fc_count = 0;
	if ( $("#checkbox-first-input").prop('checked') ) {
		fc_count++;
	}
	if ( $("#checkbox-second-input").prop('checked') ) {
		fc_count++;
	}
	if ( $("#checkbox-third-input").prop('checked') ) {
		fc_count++;
	}

	// 難易度を取得
	difficulty = $('[name=difficulty]').val();
	switch ( difficulty ) {
		case 'masterplus':
			id = 5;
			mul = 4;
			break;
		case 'master':
			id = 4;
			mul = 2;
			break;
		case 'pro':
			id = 3;
			mul = 1;
			break;
		default:
			id = 0;
			mul = 0;
			break;
	}

	// スコア計算
	score = fc_count * mul;
	if ( fc_count == 3 ) {
		score += 2;
	}

	setTimeout(exec_regist_fullcombo(id, score), 0);
}

function reset_fc_button () {
	$("#checkbox-first-input").prop('checked', false);
	$("#checkbox-first-show").html('&#x274C;');
	$("#checkbox-second-input").prop('checked', false);
	$("#checkbox-second-show").html('&#x274C;');
	$("#checkbox-third-input").prop('checked', false);
	$("#checkbox-third-show").html('&#x274C;');
	$("#checkbox-encore-input").prop('checked', false);
	$("#checkbox-encore-show").html('&#x274C;');
}
