window.fccObs = function(){};
window.fccObs.fccount = null;

function get_score () {
	onSuccess = function ( data, textStatus, jqXHR ) {
		json = JSON.parse(data);
		$(".score_value").html(json.score);
		console.log('loaded.');
		setTimeout(get_score, 1500);
	}

	onError = function ( data, textStatus, jqXHR ) {
	}

	params = (new URL(document.location)).searchParams;

	ajax_config = {
		method: 'GET',
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/users/'+params.get('user'), ajax_config);
}

function get_fc_count () {
	calculate_fc = function ( index, val ) {
		params = (new URL(document.location)).searchParams;
		if ( val.user == params.get('user') ) {
			if ( 
				((val.score == 1) && (params.get('type') == 'pro')) ||
				((val.score == 2) && (params.get('type') == 'master'))
			) {
				window.fccObs.fccount++;
			}
		}
	}

	onSuccess = function ( data, textStatus, jqXHR ) {
		json = JSON.parse(data);
		window.fccObs.fccount = 0;
		$.each(json, calculate_fc);
		$(".score_value").html(window.fccObs.fccount);
		setTimeout(get_fc_count, 2500);
	}

	onError = function ( data, textStatus, jqXHR ) {
	}

	ajax_config = {
		method: 'GET',
		success: onSuccess,
		error: onError
	};

	$.ajax('https://fc.yakitamago.info/api/v1/logs', ajax_config);
}
