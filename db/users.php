<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'abstract.php');

class FullcomboCounterUsers extends FullcomboCounter {
	private $username;
	private $password;
	private $verified;
	private $state;

	const GENERAL_NO_ERROR_STATE = 0;
	const CREATE_NOT_EMPTY_CLASS = 1;
	const CREATE_USER_ALREADY_EXISTS = 2;
	const CREATE_USERNAME_TOO_LONG = 3;
	const CREATE_PASSWORD_TOO_LONG = 4;
	const CREATE_FAILED_MAKING_PASSWORD_HASH = 5;
	const CREATE_UNKNOWN_ERROR = 6;
	const GENERAL_DATABASE_CONNECTION_ERROR = 7;
	const GENERAL_CLASS_IS_EMPTY = 8;
	const LOGIN_PASSWORD_MISSMATCH = 9;
	const UPDATE_PASSWORD_FAIL = 10;
	
	public function __construct () {
		$this->username = NULL;
		$this->password = NULL;
		$this->verified = NULL;
		$this->state = $this::GENERAL_NO_ERROR_STATE;
	}

	public function create_new_user ( $username, $password ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			if ( $this->username !== NULL ) {
				throw new Exception($this::CREATE_NOT_EMPTY_CLASS);
			} else {
				$this->get_user_information_from_database($username);
			}
			if ( $this->username !== NULL ) {
				throw new Exception($this::CREATE_USER_ALREADY_EXISTS);
			}
			if ( strlen($username) > 32 ) {
				throw new Exception($this::CREATE_USERNAME_TOO_LONG);
			}
			if ( strlen($password) > 72 ) {
				throw new Exception($this::CREATE_PASSWORD_TOO_LONG);
			}
			if ( ($hash = password_hash($password, PASSWORD_DEFAULT)) === FALSE ) {
				throw new Exception($this::CREATE_FAILED_MAKING_PASSWORD_HASH);
			}

			$query = 'INSERT INTO users (name, password, verified) VALUES (:name, :password, FALSE)';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':name', $username, PDO::PARAM_STR);
			$sth->bindValue(':password', $hash, PDO::PARAM_STR);
			$sth->execute();
			if ( $sth->rowCount() > 0 ) {
				$this->username = $username;
				$this->password = $hash;
				$this->verified = FALSE;
			} else {
				throw new Exception($this::CREATE_UNKNOWN_ERROR);
			}
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}

	public function get_user_information_from_database ( $username ) {
		try {
			if ( $this->pdo == NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			$query = 'SELECT * FROM users WHERE name = :name';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':name', $username, PDO::PARAM_STR);
			$sth->execute();
			$result = $sth->fetchAll();
			if ( count($result) > 0 ) {
				$this->username = $result[0]['name'];
				$this->password = $result[0]['password'];
				$this->verified = $result[0]['verified'];
			}
		} catch ( Exception $e ) {
		}
	}

	public function get_username () {
		return $this->username;
	}

	public function get_verified () {
		return $this->verified;
	}

	public function get_state () {
		return $this->state;
	}

	private function update_password ( $new_password_hash ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			if ( $this->username === NULL ) {
				throw new Exception($this::GENERAL_CLASS_IS_EMPTY);
			}

			$query = 'UPDATE users SET password = :newhash WHERE name = :user';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':user', $this->username, PDO::PARAM_STR);
			$sth->bindValue(':newhash', $new_password_hash, PDO::PARAM_STR);
			$sth->execute();
			if ( $sth->rowCount() != 1 ) {
				throw new Exception($this::UPDATE_PASSWORD_FAIL);
			}

			$this->password = $new_password_hash;
			$this->state = 0;
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}

	public function get_score () {
		if ( $this->pdo === NULL ) {
			throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
		}
		if ( $this->username === NULL ) {
			throw new Exception($this::GENERAL_CLASS_IS_EMPTY);
		}

		$query = 'SELECT SUM(score) FROM logs WHERE name = :name';
		$sth = $this->pdo->prepare($query);
		$sth->bindValue(':name', $this->username, PDO::PARAM_STR);
		$sth->execute();
		$result = $sth->fetchAll();

		if ( count($result) > 0 ) {
			if ( $result[0]['sum'] === NULL ) {
				return 0;
			} else {
				return $result[0]['sum'];
			}
		} else {
			return 0;
		}
	}

	public function user_authentication ( $password ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			if ( $this->username === NULL ) {
				throw new Exception($this::GENERAL_CLASS_IS_EMPTY);
			}

			if ( !password_verify($password, $this->password) ) {
				throw new Exception($this::LOGIN_PASSWORD_MISSMATCH);
			}
			if ( password_needs_rehash($this->password, PASSWORD_DEFAULT) ) {
				if ( ($hash = password_hash($password, PASSWORD_DEFAULT)) === FALSE ) {
					// ハッシュアップデート時の新ハッシュ作成の失敗だったら今回は見逃し
				} else {
					$this->update_password($hash);
				}
			}

			$login = TRUE;
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
			$login = FALSE;
		}

		return $login;
	}

	public function change_password ( $old_password, $new_password ) {
		if ( $this->pdo === NULL ) {
			throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
		}
		if ( $this->username === NULL ) {
			throw new Exception($this::GENERAL_CLASS_IS_EMPTY);
		}

		if ( !$this->user_authentication($old_password) ) {
			return FALSE;
		}

		if ( ($hash = password_hash($new_password, PASSWORD_DEFAULT)) === FALSE ) {
			return FALSE;
		}

		$this->update_password($hash);
		if ( $this->state != $this::GENERAL_NO_ERROR_STATE ) {
			return FALSE;
		}

		$this->state = $this::GENERAL_NO_ERROR_STATE;
		return TRUE;
	}
}