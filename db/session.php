<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'abstract.php');

class FullcomboCounterSession extends FullcomboCounter {
	public $name;
	public $session_id;
	public $expired_at;
	private $state;

	const GENERAL_NO_ERROR_STATE = 0;
	const GENERAL_DATABASE_CONNECTION_ERROR = 1;
	const SEARCH_SESSION_NOT_FOUND = 2;
	const SESSION_USER_NOT_FOUND = 3;
	const SESSION_CREATE_FAIL = 4;

	public function __construct () {
		$this->name = NULL;
		$this->session_id = NULL;
		$this->expired_at = NULL;
		$this->state = $this::GENERAL_NO_ERROR_STATE;
	}

	public function create_new_session ( $name ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'users.php');
			$user_instance = new FullcomboCounterUsers();
			$user_instance->set_pdo_instance($this->pdo);
			$user_instance->get_user_information_from_database($name);
			if ( $user_instance->get_username() === NULL ) {
				throw new Exception($this::SESSION_USER_NOT_FOUND);
			}

			$id = NULL;
			while ( TRUE ) {
				$id = md5(uniqid(mt_rand(), true));
				$query = 'SELECT session_id FROM sessions WHERE session_id = :id';
				if ( !isset($select_id_sth) ) {
					$select_id_sth = $this->pdo->prepare($query);
				}
				$select_id_sth->bindValue(':id', $id, PDO::PARAM_STR);
				$select_id_sth->execute();
				$result = $select_id_sth->fetchAll();
				if ( count($result) == 0 ) {
					break;
				}
			}

			$query = 'INSERT INTO sessions (session_id, name, expired_at) VALUES (:id, :name, :expired_at)';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':id', $id, PDO::PARAM_STR);
			$sth->bindValue(':name', $name, PDO::PARAM_STR);
			$exp = new DateTime();
			$exp->setTimezone(new DateTimeZone('UTC'));
			$exp->add(new DateInterval('P1D'));
			$sth->bindValue(':expired_at', $exp->format('Y-m-d H:i:s'), PDO::PARAM_STR);
			$sth->execute();
			if ( $sth->rowCount() == 0 ) {
				throw new Exception($this::SESSION_CREATE_FAIL);
			} else {
				$this->session_id = $id;
				$this->name = $name;
				$this->expired_at = $exp;
				$this->state = $this::GENERAL_NO_ERROR_STATE;
			}
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}

	public function search_session_by_id ( $id ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}

			$query = 'SELECT * FROM sessions WHERE session_id = :id';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':id', $id, PDO::PARAM_STR);
			$sth->execute();
			$result = $sth->fetchAll();
			if ( count($result) > 0 ) {
				$this->name = $result[0]['name'];
				$this->session_id = $result[0]['session_id'];
				$ea = DateTime::createFromFormat('Y-m-d H:i:s', $result[0]['expired_at'], new DateTimeZone('UTC'));
				$this->expired_at = $ea;
			} else {
				throw new Exception($this::SEARCH_SESSION_NOT_FOUND);
			}
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}

	public function get_name () {
		return $this->name;
	}

	public function get_session_id () {
		return $this->session_id;
	}

	public function get_expired_at () {
		return $this->expired_at;
	}
}