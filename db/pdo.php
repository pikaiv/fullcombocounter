<?php

function connect_database () {
	require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'settings.php');
	$dsn = "pgsql:host={$db_host};port={$db_port};dbname={$db_name}";
	try {
		$pdo = new PDO($dsn, $db_user, $db_password);
	} catch ( PDOException $e ) {
		$pdo = null;
	} catch ( Exception $e ) {
		$pdo = null;
	}
	return $pdo;
}
