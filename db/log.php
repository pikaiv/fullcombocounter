<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'abstract.php');

class FullcomboCounterLogs extends FullcomboCounter {
	public $id;
	public $user;
	public $difficulty;
	public $score;
	public $created_at;
	private $state;

	const GENERAL_NO_ERROR_STATE = 0;
	const GENERAL_DATABASE_CONNECTION_ERROR = 1;
	const FAILED_LOGS_FROM_DATABASE = 2;
	const REGIST_FAILED_NOT_FOUND_USER = 3;
	const REGIST_NOT_EMPTY_CLASS = 4;
	const REGIST_UNKNOWN_ERROR = 5;
	const ERROR_CLASS_IS_EMPTY = 6;
	const ERROR_DELETE_FAILED = 7;

	public function __construct () {
		$this->id = NULL;
		$this->user = NULL;
		$this->difficulty = new LogDifficulty();
		$this->score = NULL;
		$this->created_at = NULL;
		$this->state = $this::GENERAL_NO_ERROR_STATE;
	}

	public function set_log_data ( $id, $user, $difficulty_id, $score, $created_at ) {
		$this->id = $id;
		$this->user = $user;
		$this->difficulty->set_difficulty_id($difficulty_id);
		$this->score = $score;
		$this->created_at = $created_at;
	}

	public function regist_log ( $user, $difficulty_id, $score ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			if ( $this->id !== NULL ) {
				throw new Exception($this::REGIST_NOT_EMPTY_CLASS);
			}
			require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'users.php');
			$user_instance = new FullcomboCounterUsers();
			$user_instance->set_pdo_instance($this->pdo);
			$user_instance->get_user_information_from_database($user);
			if ( $user_instance->get_username() === NULL ) {
				throw new Exception($this::REGIST_FAILED_NOT_FOUND_USER);
			}

			$id = NULL;
			while ( TRUE ) {
				$id = md5(uniqid(mt_rand(), true));
				$query = 'SELECT id FROM logs WHERE id = :id';
				if ( !isset($select_id_sth) ) {
					$select_id_sth = $this->pdo->prepare($query);
				}
				$select_id_sth->bindValue(':id', $id, PDO::PARAM_STR);
				$select_id_sth->execute();
				$result = $select_id_sth->fetchAll();
				if ( count($result) == 0 ) {
					break;
				}
			}

			$query = 'INSERT INTO logs (id, name, difficulty, score, created_at) VALUES (:id, :name, :difficulty, :score, :created_at)';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':id', $id, PDO::PARAM_STR);
			$sth->bindValue(':name', $user, PDO::PARAM_STR);
			$sth->bindValue(':difficulty', $difficulty_id, PDO::PARAM_INT);
			$sth->bindValue(':score', $score, PDO::PARAM_INT);
			$now = new DateTime();
			$now->setTimezone(new DateTimeZone('UTC'));
			$sth->bindValue(':created_at', $now->format('Y-m-d H:i:s'), PDO::PARAM_STR);
			$sth->execute();
			if ( $sth->rowCount() > 0 ) {
				$this->set_log_data($id, $user, $difficulty_id, $score, $now);
			} else {
				throw new Exception($this::REGIST_UNKNOWN_ERROR);
			}
			$this->state = $this::GENERAL_NO_ERROR_STATE;
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}

	public function get_from_database_by_id ( $id ) {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			$query = 'SELECT * FROM logs WHERE id = :id';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':id', $id, PDO::PARAM_INT);
			$sth->execute();
			$result = $sth->fetchAll();
			if ( count($result) > 0 ) {
				$created_at_datetime = DateTime::createFromFormat('Y-m-d H:i:s', $result[0]['created_at'], new DateTimeZone('UTC'));
				$this->set_log_data($result[0]['id'], $result[0]['name'], $result[0]['difficulty'], $result[0]['score'], $created_at_datetime);
			}
			$this->state = $this::GENERAL_NO_ERROR_STATE;
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}

	public function get_all_logs_from_database () {
		$state = FullcomboCounterLogs::GENERAL_NO_ERROR_STATE;
		require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'pdo.php');
		try {
			if ( ($pdo = connect_database()) === NULL ) {
				throw new Exception(FullcomboCounterLogs::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			$query = 'SELECT * FROM logs ORDER BY created_at';
			$sth = $pdo->query($query);
			if ( ($result = $sth->fetchAll()) === FALSE ) {
				throw new Exception(FullcomboCounterLogs::FAILED_LOGS_FROM_DATABASE);
			}
			$return_data = array();
			foreach ( $result as $row ) {
				$adding = new FullcomboCounterLogs();
				$adding->set_pdo_instance($pdo);
				$created_at_datetime = DateTime::createFromFormat('Y-m-d H:i:s', $row['created_at'], new DateTimeZone('UTC'));
				$adding->set_log_data($row['id'], $row['name'], $row['difficulty'], $row['score'], $created_at_datetime);
				$return_data[] = $adding;
			}
		} catch ( Exception $e ) {
			$state = $e->getMessage();
		}
		if ( $state != FullcomboCounterLogs::GENERAL_NO_ERROR_STATE ) {
			return $state;
		} else {
			return $return_data;
		}
	}

	public function get_id () {
		return $this->id;
	}

	public function get_username () {
		return $this->user;
	}

	public function get_score () {
		return $this->score;
	}

	public function get_created_at () {
		return $this->created_at;
	}

	public function get_state () {
		return $this->state;
	}

	public function delete_me () {
		try {
			if ( $this->pdo === NULL ) {
				throw new Exception($this::GENERAL_DATABASE_CONNECTION_ERROR);
			}
			if ( $this->id === NULL ) {
				throw new Exception($this::ERROR_CLASS_IS_EMPTY);
			}

			$query = 'DELETE FROM logs WHERE id = :id';
			$sth = $this->pdo->prepare($query);
			$sth->bindValue(':id', $this->id, PDO::PARAM_STR);
			$sth->execute();
			if ( $sth->rowCount() != 1 ) {
				throw new Exception($this::ERROR_DELETE_FAILED);
			}
			$this->id = NULL;
			$this->user = NULL;
			$this->score = NULL;
			$this->created_at = NULL;
			$this->state = $this::GENERAL_NO_ERROR_STATE;
		} catch ( Exception $e ) {
			$this->state = $e->getMessage();
		}
		return $this->state;
	}
}

class LogDifficulty {
	public $difficulty_id;
	public $difficulty_name;

	const DIFFICULTY_ID_UNKNOWN = 0;
	const DIFFICULTY_ID_EASY = 1;
	const DIFFICULTY_ID_NORMAL = 2;
	const DIFFICULTY_ID_PRO = 3;
	const DIFFICULTY_ID_MASTER = 4;
	const DIFFICULTY_ID_MASTERPLUS = 5;
	const DIFFICULTY_ID_PIANO = 6;
	const DIFFICULTY_ID_FORTE = 7;
	const DIFFICULTY_ID_LIGHT = 8;
	const DIFFICULTY_ID_TRICK = 9;

	const DIFFICULTY_STRING_UNKNOWN = 'UNKNOWN';
	const DIFFICULTY_STRING_EASY = 'EASY';
	const DIFFICULTY_STRING_NORMAL = 'NORMAL';
	const DIFFICULTY_STRING_PRO = 'PRO';
	const DIFFICULTY_STRING_MASTER = 'MASTER';
	const DIFFICULTY_STRING_MASTERPLUS = 'MASTER+';
	const DIFFICULTY_STRING_PIANO = 'PIANO';
	const DIFFICULTY_STRING_FORTE = 'FORTE';
	const DIFFICULTY_STRING_LIGHT = 'LIGHT';
	const DIFFICULTY_STRING_TRICK = 'TRICK';

	function __construct () {
		$this->difficulty_id = LogDifficulty::DIFFICULTY_ID_UNKNOWN;
		$this->difficulty_name = LogDifficulty::DIFFICULTY_STRING_UNKNOWN;
	}

	public function set_difficulty_id ( $id ) {
		switch ( $id ) {
			case $this::DIFFICULTY_ID_UNKNOWN:
				$this->difficulty_name = $this::DIFFICULTY_STRING_UNKNOWN;
			break;

			case $this::DIFFICULTY_ID_EASY:
				$this->difficulty_name = $this::DIFFICULTY_STRING_EASY;
			break;

			case $this::DIFFICULTY_ID_NORMAL:
				$this->difficulty_name = $this::DIFFICULTY_STRING_NORMAL;
			break;

			case $this::DIFFICULTY_ID_PRO:
				$this->difficulty_name = $this::DIFFICULTY_STRING_PRO;
			break;

			case $this::DIFFICULTY_ID_MASTER:
				$this->difficulty_name = $this::DIFFICULTY_STRING_MASTER;
			break;

			case $this::DIFFICULTY_ID_MASTERPLUS:
				$this->difficulty_name = $this::DIFFICULTY_STRING_MASTERPLUS;
			break;

			case $this::DIFFICULTY_ID_PIANO:
				$this->difficulty_name = $this::DIFFICULTY_STRING_PIANO;
			break;

			case $this::DIFFICULTY_ID_FORTE:
				$this->difficulty_name = $this::DIFFICULTY_STRING_FORTE;
			break;

			case $this::DIFFICULTY_ID_LIGHT:
				$this->difficulty_name = $this::DIFFICULTY_STRING_LIGHT;
			break;

			case $this::DIFFICULTY_ID_TRICK:
				$this->difficulty_name = $this::DIFFICULTY_STRING_TRICK;
			break;

			default:
				$this->difficulty_name = $this::DIFFICULTY_STRING_UNKNOWN;
				$id = $this::DIFFICULTY_ID_UNKNOWN;
			break;
		}

		$this->difficulty_id = $id;
	}
}
