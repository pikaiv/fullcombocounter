<?php

define('HTTP_STATUS_FORBIDDEN', 403);
define('HTTP_STATUS_NOT_FOUND', 404);
define('HTTP_STATUS_INVALID_ARGUMENT', 422);
define('HTTP_STATUS_INTERNAL_SERVER_ERROR', 500);

function return_error_http ($status)
{
	switch ( $status ) {
		case HTTP_STATUS_FORBIDDEN:
			header('HTTP', TRUE, 403);
		break;

		case HTTP_STATUS_NOT_FOUND:
			header('HTTP', TRUE, 404);
		break;

		case HTTP_STATUS_INVALID_ARGUMENT:
			header('HTTP', TRUE, 422);
		break;

		case HTTP_STATUS_INTERNAL_SERVER_ERROR:
			header('HTTP', TRUE, 500);
		break;

		default:
			header('HTTP', TRUE, 500);
		break;
	}
}
