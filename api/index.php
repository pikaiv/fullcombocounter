<?php
header('Access-Control-Allow-Origin: *');
/* 
生のPHPでREST APIっぽいルーティングを作る by Osamu Nagayama (@naga3)
https://qiita.com/naga3/items/030f757ed413515551db
*/
$paths = explode('/', $_SERVER['REQUEST_URI']);
array_shift($paths);
$api_version = trim($paths[1]);
$command = array_slice($paths, 2);

require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/pdo.php');
$pdo = connect_database();

switch ( $api_version ) {
	case 'v1':
		api_version_v1($command);
	break;

	default:
	break;
}

function api_version_v1 ($command)
{
	require_once('error.php');

	switch ( $command[0] ) {
		case 'users':
			if ( count($command) < 2 ) {
				return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			}
			require_once('users.php');
			switch ( strtolower($_SERVER['REQUEST_METHOD']) ) {
				case 'get':
					get_user_score($command[1]);
				break;

				case 'post':
					regist_user_fullcombo($command[1]);
				break;

				default:
				break;
			}
		break;

		case 'logs':
			require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'logs.php');
			switch ( strtolower($_SERVER['REQUEST_METHOD']) ) {
				case 'get':
					if ( count($command) < 2 ) {
						get_all_log();
					} else {
						get_log($command[1]);
					}
				break;

				case 'delete':
					if ( count($command) < 2 ) {
						return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
					}
					delete_log($command[1]);
				break;

				default:
				break;
			}
		break;

		case 'login':
			require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'login.php');
			switch ( strtolower($_SERVER['REQUEST_METHOD']) ) {
				case 'post':
					login_authentication();
				break;

				default:
				break;
			}
		break;

		default:
			return_error_http(HTTP_STATUS_NOT_FOUND);
		break;
	}
}

