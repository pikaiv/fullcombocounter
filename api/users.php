<?php

function get_user_score ( $username )
{
	global $pdo;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/users.php');
	require_once('error.php');
	$instance = new FullcomboCounterUsers();
	$instance->set_pdo_instance($pdo);
	$instance->get_user_information_from_database($username);
	$output = array();
	if ( $instance->get_username() === NULL ) {
		return_error_http(HTTP_STATUS_NOT_FOUND);
		$output['error'] = 'User not found.';
	} else {
		$output = array('user'=>$instance->get_username(), 'score'=>$instance->get_score());
	}
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}

function regist_user_fullcombo ( $username )
{
	global $pdo;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/session.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/log.php');
	require_once('error.php');

	try {
		$token = NULL;
		foreach ( getallheaders() as $header_name => $header_value ) {
			if ( $header_name == 'Authorization' ) {
				if ( strpos($header_value, 'Bearer ') !== 0 ) {
					return_error_http(HTTP_STATUS_FORBIDDEN);
					$output['error'] = 'Authentication Error';
					$output['error_detail'] = array();
					$output['error_detail'][] = 'ヘッダでのトークンの指定が異常です。';
					throw new Exception();
				}
				$token = substr($header_value, 7);
				break;
			}
		}
		if ( ($token === NULL) || (strlen($token) != 32) ) {
			return_error_http(HTTP_STATUS_FORBIDDEN);
			$output['error'] = 'Authentication Error';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'ヘッダにトークンが含まれていないか、形式が異常です。';
			throw new Exception();
		}

		$session_instance = new FullcomboCounterSession();
		$session_instance->set_pdo_instance($pdo);
		$session_instance->search_session_by_id($token);
		if ( $session_instance->get_name() != $username ) {
			return_error_http(HTTP_STATUS_FORBIDDEN);
			$output['error'] = 'Authentication Error';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'そのトークンは別の人のものです。';
			throw new Exception();
		}

		if ( !isset($_POST['difficulty']) ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Invalid Argument';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'POST フィールドに difficulty が含まれていません。';
			throw new Exception();
		}
		$adding_difficulty = intval($_POST['difficulty']);
		if ( !isset($_POST['score']) ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Invalid Argument';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'POST フィールドに score が含まれていません。';
			throw new Exception();
		}
		$adding_score = intval($_POST['score']);
		if ( ($adding_score < 0) || ($adding_score > 14) ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Invalid Argument';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'スコアが 0 から 14 ではありません。';
			throw new Exception();
		}

		$adding_log = new FullcomboCounterLogs();
		$adding_log->set_pdo_instance($pdo);
		$adding_log->regist_log($username, $adding_difficulty, $adding_score);

		if ( $adding_log->get_state() != FullcomboCounterLogs::GENERAL_NO_ERROR_STATE ) {
			return_error_http(HTTP_STATUS_INTERNAL_SERVER_ERROR);
			$output['error'] = 'Failed to regist log.';
			throw new Exception();
		}

		$output = $adding_log;
	} catch ( Exception $e ) {
	}
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}
