<?php

function login_authentication () {
	global $pdo;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/users.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/session.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.php');
	include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'debug.php');

	try {
		if ( !isset($_POST['user']) ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Missing user parameter';
			throw new Exception();
		}
		if ( !isset($_POST['password']) ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Missing password parameter';
			throw new Exception();
		}

		$user = $_POST['user'];
		$password = $_POST['password'];

		$user_instance = new FullcomboCounterUsers();
		$user_instance->set_pdo_instance($pdo);
		$user_instance->get_user_information_from_database($user);
		if ( $user_instance->get_username() === NULL ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Incorrect Login Information';
			if ( $debug_mode ) {
				$output['error_detail'] = array();
				$output['error_detail'][] = 'User Not Found';
			}
			throw new Exception();
		}

		if ( !$user_instance->user_authentication($password) ) {
			return_error_http(HTTP_STATUS_INVALID_ARGUMENT);
			$output['error'] = 'Incorrect Login Information';
			if ( $debug_mode ) {
				$output['error_detail'] = array();
				$output['error_detail'][] = 'Wrong Password';
			}
			throw new Exception();
		}

		$session = new FullcomboCounterSession();
		$session->set_pdo_instance($pdo);
		$session->create_new_session($user);
		if ( $session->session_id === NULL ) {
			return_error_http(HTTP_STATUS_INTERNAL_SERVER_ERROR);
			$output['error'] = "Can't create a session";
			throw new Exception();
		}

		$output = $session;
	} catch ( Exception $e ) {
	}

	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}
