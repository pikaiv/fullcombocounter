<?php

function get_all_log ( ) {
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/log.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.php');

	$output = FullcomboCounterLogs::get_all_logs_from_database();
	if ( !is_array($output) ) {
		return_error_http(HTTP_STATUS_INTERNAL_SERVER_ERROR);
		$output = array();
		$output['error'] = "Can't get logs from database.";
	}
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}

function get_log ( $logid ) {
	global $pdo;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/log.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.php');

	$output = new FullcomboCounterLogs();
	$output->set_pdo_instance($pdo);
	$output->get_from_database_by_id($logid);
	if ( $output->get_state() != FullcomboCounterLogs::GENERAL_NO_ERROR_STATE ) {
		$output = array();
		$output['error'] = "Can't find id from database.";
	}
	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}

function delete_log ( $logid ) {
	global $pdo;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/session.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../db/log.php');
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'error.php');

	try {
		$token = NULL;
		foreach ( getallheaders() as $header_name => $header_value ) {
			if ( $header_name == 'Authorization' ) {
				if ( strpos($header_value, 'Bearer ') !== 0 ) {
					return_error_http(HTTP_STATUS_FORBIDDEN);
					$output['error'] = 'Authentication Error';
					$output['error_detail'] = array();
					$output['error_detail'][] = 'ヘッダでのトークンの指定が異常です。';
					throw new Exception();
				}
				$token = substr($header_value, 7);
				break;
			}
		}
		if ( ($token === NULL) || (strlen($token) != 32) ) {
			return_error_http(HTTP_STATUS_FORBIDDEN);
			$output['error'] = 'Authentication Error';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'ヘッダにトークンが含まれていないか、形式が異常です。';
			throw new Exception();
		}

		$session_instance = new FullcomboCounterSession();
		$session_instance->set_pdo_instance($pdo);
		$session_instance->search_session_by_id($token);

		$log_instance = new FullcomboCounterLogs();
		$log_instance->set_pdo_instance($pdo);
		$log_instance->get_from_database_by_id($logid);
		if ( $log_instance->get_state() != FullcomboCounterLogs::GENERAL_NO_ERROR_STATE ) {
			$output = array();
			$output['error'] = "Can't find id from database.";
			throw new Exception();
		}

		if ( $session_instance->get_name() != $log_instance->get_username() ) {
			return_error_http(HTTP_STATUS_FORBIDDEN);
			$output['error'] = 'Authentication Error';
			$output['error_detail'] = array();
			$output['error_detail'][] = 'そのトークンは別の人のものです。';
			throw new Exception();
		}

		$log_instance->delete_me();
		if ( $log_instance->get_state() != FullcomboCounterLogs::GENERAL_NO_ERROR_STATE ) {
			$output = array();
			$output['error'] = "Can't delete log from database.";
			throw new Exception();
		}
		$output['state'] = 'success';
	} catch ( Exception $e ) {
	}

	echo json_encode($output, JSON_UNESCAPED_UNICODE);
}
